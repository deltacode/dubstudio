# DubStudio

DubStudio est un logiciel de doublage audio sur vidéo.

# Le principe
Toutes vos séries et films sont doublés, afin de préserver une bonne qualité sonore et de proposer une multitude de langages pour le visionnage. 

Que ce soit pour des raisons professionnelles, ou du montage à la maison, DubStudio propose une solution efficace, légère et simple d'utilisation.

Cela peut-être un passe-temps amusant ! Incarnez Marty dans Retour vers le futur ou Gru dans moi, moche et méchant, tout est possible !

# Fonctionnement
Pour démarrer un projet, vous devez importer la vidéo sur laquelle vous voulez faire le doublage.
Vous ajoutez ensuite les personnages à doubler.
Ensuite, vous pouvez placer les répliques sur une timeline.
Lorsque vous jouerez la vidéo, vous n'aurez qu'à lire les répliques, comme dans un karaoké !


# Fonctionnalités
*  Importation d'une vidéo d'appui
*  Création de plusieurs personnages
*  Création de plusieurs scènes
*  De nombreux paramètres afin de personnaliser votre utilisation