﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DubStudio
{
    /// <summary>
    /// Logique d'interaction pour AddActionBox.xaml
    /// </summary>
    public partial class AddActionBox : Window
    {
        //private MainWindow handler;
        //private bool mode = false;
        //private object m_element;

        //public enum ElementType
        //{
        //    Character,
        //    Scene,
        //    Text
        //}

        //public AddActionBox(MainWindow e, AddActionBox.ElementType t)
        //{
        //    InitializeComponent();
        //    handler = e;
        //    mode = false;
        //    btn_color_char.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 0, 0));
        //    List<string> shortcutList = new List<string> { "Aucun", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12" };
        //    btn_short_char.ItemsSource = shortcutList;
        //    List<string> charactersList = new List<string> { };
        //    foreach (Character a in Character.getCharacters()) { charactersList.Add(a.getName()); }
        //    txt_character.ItemsSource = charactersList;
        //    foreach (System.Windows.Media.FontFamily fontFamily in Fonts.SystemFontFamilies)
        //    {
        //        //Chargement des polices
        //        txt_font_family.Items.Add(fontFamily.Source);
        //    }
        //    if (t == AddActionBox.ElementType.Character) { elementTab.SelectedIndex = 0; txt_name_char.Focus(); }
        //    else if (t == AddActionBox.ElementType.Scene) { elementTab.SelectedIndex = 1; txt_id_scene.Focus(); }
        //    else if (t == AddActionBox.ElementType.Text) { elementTab.SelectedIndex = 2; txt_text_text.Focus(); }
        //}

        //public AddActionBox(MainWindow e, AddActionBox.ElementType t, object element)
        //{
        //    InitializeComponent();
        //    handler = e;
        //    mode = true;
        //    m_element = element;
        //    tab_item_character.IsEnabled = false;
        //    tab_item_scene.IsEnabled = false;
        //    tab_item_text.IsEnabled = false;
        //    btn_color_char.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 0, 0));
        //    List<string> shortcutList = new List<string> { "Aucun", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12" };
        //    btn_short_char.ItemsSource = shortcutList;
        //    List<string> charactersList = new List<string> { };
        //    foreach (Character a in Character.getCharacters()) { charactersList.Add(a.getName()); }
        //    foreach (System.Windows.Media.FontFamily fontFamily in Fonts.SystemFontFamilies)
        //    {
        //        //Chargement des polices
        //        txt_font_family.Items.Add(fontFamily.Source);
        //    }
        //    txt_character.ItemsSource = charactersList;
        //    this.Title = "Modification d'un élément";
        //    if (t == AddActionBox.ElementType.Character)
        //    {
        //        elementTab.SelectedIndex = 0;
        //        txt_name_char.Focus();
        //        Character a = (Character)element;
        //        txt_name_char.Text = a.getName();
        //        btn_color_char.Background = new SolidColorBrush(a.getColor());
        //        btn_short_char.Text = a.getShortcut();

        //    }
        //    else if (t == AddActionBox.ElementType.Scene)
        //    {
        //        elementTab.SelectedIndex = 1;
        //        txt_id_scene.Focus();
        //        Scene a = (Scene)element;
        //        txt_id_scene.Text = a.getName();
        //        btn_start_scene.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0,255,0));
        //        btn_end_scene.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //        startScene = a.getStart();
        //        endScene = a.getEnd();
        //        System.Windows.Controls.ToolTip t1 = new System.Windows.Controls.ToolTip();
        //        t1.Content = String.Format("{0} | {1}", startScene.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //        btn_start_scene.ToolTip = t1;
        //        System.Windows.Controls.ToolTip t2 = new System.Windows.Controls.ToolTip();
        //        t2.Content = String.Format("{0} | {1}", endScene.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //        btn_end_scene.ToolTip = t2;
        //    }
        //    else if (t == AddActionBox.ElementType.Text)
        //    {
        //        elementTab.SelectedIndex = 2;
        //        txt_text_text.Focus();
        //        Replica a = (Replica)element;
        //        txt_text_text.Text = a.getText();
        //        txt_character.Text = a.getCharacter().getName();
        //        btn_start_text.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //        btn_end_text.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //        startText = a.getStart();
        //        endText = a.getEnd();
        //        txt_font_family.SelectedItem = a.getFont().Name;
        //        txt_font_size.Text = a.getFont().Size.ToString();
        //        txt_font_style.SelectedIndex = (a.getFont().Style == System.Drawing.FontStyle.Regular) ? 0 : (a.getFont().Style == System.Drawing.FontStyle.Bold) ? 1 : (a.getFont().Style == System.Drawing.FontStyle.Italic) ? 2 : 3;
        //        System.Windows.Controls.ToolTip t1 = new System.Windows.Controls.ToolTip();
        //        t1.Content = String.Format("{0} | {1}", startText.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //        btn_start_text.ToolTip = t1;
        //        System.Windows.Controls.ToolTip t2 = new System.Windows.Controls.ToolTip();
        //        t2.Content = String.Format("{0} | {1}", endText.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //        btn_end_text.ToolTip = t2;
        //        txt_track.Text = a.getTrack().ToString();
        //    }
        //}

        //private void btnQuit_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}

        //private bool windowValidation()
        //{
        //    SolidColorBrush btnColor = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
        //    if (elementTab.SelectedIndex == 0)
        //    {
        //        if (txt_name_char.Text != "" && txt_name_char.Text != null && btn_short_char.Text != "" && btn_short_char.Text != null) return true;
        //        else return false;
        //    }
        //    else if (elementTab.SelectedIndex == 1)
        //    {
        //        if (txt_id_scene.Text != "" && txt_id_scene.Text != null && btn_start_scene.Background != btnColor && btn_end_scene.Background != btnColor) return true;
        //        else return false;
        //    }
        //    else if (elementTab.SelectedIndex == 2)
        //    {
        //        if (txt_text_text.Text != "" && txt_text_text.Text != null && txt_character.Text != "" && txt_character.Text != null && txt_track.Text != "" && txt_track.Text != null && btn_start_text.Background != btnColor && btn_end_text.Background != btnColor) return true;
        //        else return false;
        //    }
        //    else return false;
        //}

        //private void btnValid_Click(object sender, RoutedEventArgs e)
        //{
        //    //Ajout / Modification de l'élément
        //    SolidColorBrush color = (SolidColorBrush)btn_color_char.Background;
        //    //Création d'une Action correspondant à l'élément et rechargement des data-grids
        //    if (windowValidation())
        //    {
        //        if (!mode)
        //        {
        //            //Mode ajout
        //            switch (elementTab.SelectedIndex)
        //            {
        //                case 0:
        //                    Action.addAction(new Action("P", new Character(1, txt_name_char.Text, color.Color, btn_short_char.Text), Action.Type.Addition, false), handler);
        //                    handler.displayCharacters();
        //                    handler.displayTexts();
        //                    break;
        //                case 1:
        //                    Action.addAction(new Action("S", new Scene(txt_id_scene.Text, startScene, endScene), Action.Type.Addition, false), handler);
        //                    handler.displayScenes();
        //                    break;
        //                case 2:
        //                    Font font = new Font(txt_font_family.SelectedItem.ToString(), int.Parse(txt_font_size.Text.ToString()));
        //                    Action.addAction(new Action("T", new Replica(Character.getCharacters(txt_character.SelectedIndex), txt_text_text.Text, txt_track.SelectedIndex + 1, font, startText, endText), Action.Type.Addition, false), handler);
        //                    handler.displayTexts();
        //                    break;
        //            }
        //        }
        //        else
        //        {
        //            //Mode modification
        //            switch (elementTab.SelectedIndex)
        //            {
        //                case 0:
        //                    Character a = (Character)m_element;
        //                    Action.addAction(new Action("P", a, new Character(1, txt_name_char.Text, color.Color, btn_short_char.Text), Action.Type.Modification, false), handler);
        //                    handler.displayCharacters();
        //                    handler.displayTexts();
        //                    break;
        //                case 1:
        //                    Scene b = (Scene)m_element;
        //                    Action.addAction(new Action("S", b, new Scene(txt_id_scene.Text, startScene, endScene), Action.Type.Modification, false), handler);
        //                    handler.displayScenes();
        //                    break;
        //                case 2:
        //                    Font font = new Font(txt_font_family.SelectedItem.ToString(), int.Parse(txt_font_size.Text.ToString()));
        //                    Replica c = (Replica)m_element;
        //                    Action.addAction(new Action("T", c, new Replica(Character.getCharacters(txt_character.SelectedIndex), txt_text_text.Text, txt_track.SelectedIndex + 1, font, startText, endText), Action.Type.Modification, false), handler);
        //                    handler.displayTexts();
        //                    break;
        //            }
        //        }
        //        this.Close();
        //    }
        //    else System.Windows.MessageBox.Show("Vous devez remplir tous les champs avant de pouvoir ajouter ou modifier cet élément");
        //}

        //TimeSpan startScene;
        //TimeSpan endScene;
        //private void start_scene_Click(object sender, RoutedEventArgs e)
        //{
        //    startScene = new TimeSpan(handler.video_player.Position.Ticks);
        //    btn_start_scene.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0,255,0));
        //    System.Windows.Controls.ToolTip t = new System.Windows.Controls.ToolTip();
        //    t.Content = String.Format("{0} | {1}", handler.video_player.Position.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //    btn_start_scene.ToolTip = t;
        //}
        //private void end_scene_Click(object sender, RoutedEventArgs e)
        //{
        //    endScene = new TimeSpan(handler.video_player.Position.Ticks);
        //    btn_end_scene.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //    System.Windows.Controls.ToolTip t = new System.Windows.Controls.ToolTip();
        //    t.Content = String.Format("{0} | {1}", handler.video_player.Position.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //    btn_end_scene.ToolTip = t;
        //}

        //TimeSpan startText;
        //TimeSpan endText;
        //private void start_text_Click(object sender, RoutedEventArgs e)
        //{
        //    startText = new TimeSpan(handler.video_player.Position.Ticks);
        //    btn_start_text.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //    System.Windows.Controls.ToolTip t = new System.Windows.Controls.ToolTip();
        //    t.Content = String.Format("{0} | {1}", handler.video_player.Position.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //    btn_start_text.ToolTip = t;
        //}
        //private void end_text_Click(object sender, RoutedEventArgs e)
        //{
        //    endText = new TimeSpan(handler.video_player.Position.Ticks);
        //    btn_end_text.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
        //    System.Windows.Controls.ToolTip t = new System.Windows.Controls.ToolTip();
        //    t.Content = String.Format("{0} | {1}", handler.video_player.Position.ToString(@"hh\:mm\:ss\.fff"), handler.video_player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss\.fff"));
        //    btn_end_text.ToolTip = t;
        //}


        //private void btn_color_char_Click(object sender, RoutedEventArgs e)
        //{
        //    //Choix de la couleur
        //    ColorDialog colorDialog = new ColorDialog();
        //    colorDialog.FullOpen = true;
        //    colorDialog.AnyColor = true;
        //    colorDialog.ShowDialog();
        //    btn_color_char.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(colorDialog.Color.R, colorDialog.Color.G, colorDialog.Color.B));
        //}
    }
}
