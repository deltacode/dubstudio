﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DubStudio
{
    public class Actor
    {
        private int _actorsId = 0;

        private int _id;
        private String _name;
        private Gender _gender;
        private int _age;
        private List<Character> _characters;

        public enum Gender
        {
            MALE = 0,
            FEMALE = 1
        }

        public Actor()
        {
            this._id = _actorsId++;
            this._name = Application.Current.Resources["S_ACTOR_BLANK"].ToString();
            this._gender = Gender.MALE;
            this._age = 0;
            _characters = new List<Character> { };
        }

        public Actor(String name, Gender gender, int age)
        {
            this._id = _actorsId++;
            this._name = name;
            this._gender = gender;
            this._age = age;
            _characters = new List<Character> { };
        }

        public Actor(String name, Gender gender, int age, List<Character> characters)
        {
            this._id = _actorsId++;
            this._name = name;
            this._gender = gender;
            this._age = age;
            _characters = characters;
        }

        public int getId()
        {
            return this._id;
        }

        public String getName()
        {
            return _name;
        }

        public void setName(String name)
        {
            this._name = name;
        }

        public Gender getGender()
        {
            return _gender;
        }

        public void setGender(Gender gender)
        {
            this._gender = gender;
        }

        public int getAge()
        {
            return _age;
        }

        public void setAge(int age)
        {
            this._age = age;
        }

        public List<Character> getCharacters()
        {
            return this._characters;
        }

        public void setCharacters(List<Character> characters)
        {
            this._characters = characters;
        }

        public void clearCharacters()
        {
            this._characters.Clear();
        }

        public void addCharacter(Character character)
        {
            this._characters.Add(character);
        }

        public void removeCharacter(Character character)
        {
            this._characters.Remove(character);
        }
    }
}
