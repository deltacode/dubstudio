﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Data.SQLite;
using System.IO;
using MessageBox = System.Windows.MessageBox;
using System.Data;
using Sentry;
using System.Collections.ObjectModel;
using System.ComponentModel;
using BrendanGrant.Helpers.FileAssociation;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;

namespace DubStudio
{
    public partial class DSWindow : Window
    {
        private Project _project;
        private DispatcherTimer timer;

        public DSWindow(string projectPath = null)
        {
            InitializeComponent();
            setControlsEnabled(false);
            if (projectPath != null) openProject(new Uri(projectPath, UriKind.Absolute));
        }

        /*Ajout du projet actuel à la liste des projets ouverts récemment*/
        public void addToRecentProjects(Project p)
        {
            StringCollection recentProjects = Properties.Settings.Default.recentProjects;
            recentProjects.Insert(0, p.getName() + ";" + p.getPath());
            if (recentProjects.Count > Properties.Settings.Default.recentProjectsBuffer) recentProjects.RemoveAt(recentProjects.Count - 1);
            Properties.Settings.Default.recentProjects = recentProjects;
            Properties.Settings.Default.Save();
        }

        /*Réactualise la liste des projets récents*/
        public void refreshRecentProjects()
        {
            menu_project_recent.Items.Clear();
            StringCollection recentProjects = Properties.Settings.Default.recentProjects;
            if (recentProjects.Count == 0) menu_project_recent.IsEnabled = false;
            else
            {
                foreach (string s in recentProjects)
                {
                    string[] projectInformation = s.Split(';');
                    System.Windows.Controls.MenuItem recent = new System.Windows.Controls.MenuItem();
                    string tooltipMessage = "";
                    if (!File.Exists(projectInformation[1])) {
                        recent.Icon = new System.Windows.Controls.Image { Source = new BitmapImage(new Uri("pack://application:,,,/DubStudio;component/assets/img/x64/warning.png", UriKind.Absolute)) };
                        tooltipMessage = (string)System.Windows.Application.Current.Resources["S_MENU_PROJECT_RECENT_LOST"] + "\n";
                    }
                    recent.Header = projectInformation[0];
                    recent.ToolTip = tooltipMessage + projectInformation[1];
                    recent.Click += openRecentProject;
                    menu_project_recent.Items.Add(recent);
                }
                menu_project_recent.IsEnabled = true;
            }
        }

        /*Clic sur l'item d'un projet récent*/
        public void openRecentProject(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem item = (System.Windows.Controls.MenuItem)sender;
            string path = item.ToolTip.ToString();
            if (path.Contains((string)System.Windows.Application.Current.Resources["S_MENU_PROJECT_RECENT_LOST"])) MessageBox.Show((string)System.Windows.Application.Current.Resources["S_DIALOG_PROJECT_LOST_MESSAGE"], (string)System.Windows.Application.Current.Resources["S_DIALOG_PROJECT_LOST_TITLE"], MessageBoxButton.OK, MessageBoxImage.Error);
            else openProject(new Uri(path, UriKind.Absolute));
        }

        public void setControlsEnabled(bool enabled)
        {
            video_player.enablingControls(enabled);
            if (!enabled) video_player.showVideoSelector(false);
            timeline.enablingControls(enabled);
            //tab_control.IsEnabled = enabled;
            menu_edition.IsEnabled = enabled;
            menu_project_save.IsEnabled = enabled;
            menu_project_save_as.IsEnabled = enabled;
            menu_project_close.IsEnabled = enabled;
        }

        public Project getProject()
        {
            return _project;
        }

        /*Crée un nouveau projet*/
        public void newProject()
        {
            status_loading.Visibility = Visibility.Visible;
            _project = new Project();
            loadProject();
            status_label.Content = (string)System.Windows.Application.Current.Resources["S_STATUS_PROJECT_CREATED"];
            status_loading.Visibility = Visibility.Collapsed;
        }

        /*Ouvre un projet existant depuis une boîte de dialogue*/
        public void openProject(Uri projectPath = null)
        {
            //Si le chemin du projet n'est pas spécifié, on le sélectionne depuis la boite de dialogue
            status_loading.Visibility = Visibility.Visible;
            if (projectPath == null)
            {
                status_label.Content = (string)System.Windows.Application.Current.Resources["S_STATUS_PROJECT_OPENING"];
                OpenFileDialog openDialog = new OpenFileDialog();
                openDialog.Title = System.Windows.Application.Current.Resources["S_DIALOG_OPEN_PROJECT_TITLE"].ToString();
                openDialog.Filter = System.Windows.Application.Current.Resources["S_DIALOG_OPEN_PROJECT_FILTER"].ToString() + " | *.dub;";
                openDialog.Multiselect = false;
                if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    projectPath = new Uri(openDialog.FileName, UriKind.Absolute);
                    //Chargement du project depuis l'URL donnée
                    _project = new Project();
                    loadProject();
                    status_label.Content = (string)System.Windows.Application.Current.Resources["S_STATUS_PROJECT_OPENED"];
                    status_loading.Visibility = Visibility.Collapsed;
                }
                else
                {
                    status_label.Content = (string)System.Windows.Application.Current.Resources["S_STATUS_READY"];
                    status_loading.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                //Chargement du project depuis l'URL donnée
                _project = new Project();
                loadProject();
                status_label.Content = (string)System.Windows.Application.Current.Resources["S_STATUS_PROJECT_OPENED"];
                status_loading.Visibility = Visibility.Collapsed;
            }
        }

        /*Charge le projet dans le logiciel*/
        public void loadProject()
        {
            this.Title = "DubStudio - " + _project.getName() + " (" + _project.getPath() + ")";
            if (_project.getVideoPath() == null) video_player.showVideoSelector(true);
            else
            {
                video_player.showVideoSelector(false);
                video_player.loadVideo(_project.getVideoPath());
                //Application du timer pour la vidéo
                if(!timer.IsEnabled) timer.Start();
            }
            addToRecentProjects(_project);

            //Import de données test
            /*Actor clement = new Actor("Clément", Actor.Gender.MALE, 22);
            _project.addActor(clement);
            Character marty = new Character("Marty", Character.Gender.MALE, 22, System.Windows.Media.Color.FromRgb(12, 54, 78));
            clement.addCharacter(marty);
            Replica rep1 = new Replica("Mais c'est une vraie poule mouillée !", 1, new TimeSpan(0,0,0), new TimeSpan(0,0,30));
            marty.addReplica(rep1);

            Actor valentin = new Actor("Valentin", Actor.Gender.MALE, 24);
            _project.addActor(valentin);
            Character doc = new Character("Doc", Character.Gender.MALE, 54, System.Windows.Media.Color.FromRgb(0, 0, 220));
            valentin.addCharacter(doc);
            Replica rep2 = new Replica("Nom de Zeus !", 2, new TimeSpan(0, 0, 20), new TimeSpan(0, 0, 40));
            doc.addReplica(rep2);*/

            //Import des données
            /*data_characters.Items.Clear();
            foreach (Character c in p.getCharacters())
            {  
                data_characters.Items.Add(new DataCharacter(c.getId(), c.getName(), new SolidColorBrush(c.getColor()), c.getShortcut()));
            }
            //Import des répliques
            data_replicas.Items.Clear();
            DataReplica.updateCharacters(p.getCharacters());
            foreach (Replica r in p.getReplicas())
            {
                data_replicas.Items.Add(new DataReplica(r.getId(), r.getText(), r.getCharacter().getName(), r.getStart().ToString(@"hh\:mm\:ss\.fff"), r.getEnd().ToString(@"hh\:mm\:ss\.fff")));
            }*/

            setControlsEnabled(true);
        }

        /*Fermeture du projet*/
        public void closeProject()
        {
            if (timer.IsEnabled) timer.Stop();
            _project = null;
            setControlsEnabled(false);
            this.Title = "DubStudio";
            refreshRecentProjects();
        }

        public void saveProject()
        {
            /* - Sérialisation du projet et enregistrement dans un fichier
             * - Enregistrement du nouveau chemin d'accès dans le projet
             */
            _project.hasUnsavedChanges(false);
        }

        public void saveAsProject()
        {
            /* - Sérialisation du projet et enregistrement dans un autre fichier
             * - Enregistrement du nouveau chemin d'accès dans le projet
             */
            _project.hasUnsavedChanges(false);
        }

        /*Quitter le logiciel*/
        public void quitApplication(CancelEventArgs e)
        {
            if(_project != null && _project.hasUnsavedChanges())
            {
                MessageBoxResult msgQuit = MessageBox.Show((string)System.Windows.Application.Current.Resources["S_DIALOG_QUIT_UNSAVED_MESSAGE"], (string)System.Windows.Application.Current.Resources["S_DIALOG_QUIT_UNSAVED_TITLE"], MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                if(msgQuit == MessageBoxResult.OK)
                {
                    e.Cancel = false;
                    saveProject();
                }
                else if(msgQuit == MessageBoxResult.No)
                {
                    e.Cancel = false;
                    System.Windows.Application.Current.Shutdown();
                }
                else e.Cancel = true;
            }
        }

        /*Evènements des contrôles*/
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            /*Affiche les projets ouverts récemment*/
            refreshRecentProjects();
            /*Demande de restauration projet*/
            string executable = System.Windows.Forms.Application.StartupPath;
            Directory.CreateDirectory(System.IO.Path.Combine(executable, "Temp"));
            string[] files = Directory.GetFiles(System.IO.Path.Combine(executable, "Temp"));
            if (files.Length != 0 && _project == null)
            {
                MessageBoxResult msgRestore = MessageBox.Show((string)System.Windows.Application.Current.Resources["S_DIALOG_CLEAN_TEMP"], (string)System.Windows.Application.Current.Resources["S_TITLE_CLEAN_TEMP"], MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msgRestore == MessageBoxResult.Yes) openProject(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), files[0]), UriKind.Absolute));
            }
            /*Démarrer avec le dernier projet si l'option est activée*/
            else if (Properties.Settings.Default.startWithRecentProject) openProject(new Uri(Properties.Settings.Default.recentProjects[0].Split(';')[1], UriKind.Absolute));
            
            //Préparation du timer de rafraîchissement
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(25);
            timer.Tick += refreshPosition;
        }

        /*Récupère la positon de la vidéo, met à jour la position et les répliques dans la timeline*/
        public void refreshPosition(object sender, EventArgs e)
        {
            //Met à jour la position du lecteur embarqué
            video_player.updateVideoTime();
            TimeSpan videoPos = video_player.getVideoPosition();
            timeline.synchronizeReplicas(videoPos, _project.getActors());
        }

        /*Réadaptation lors du redimensionnement*/
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            timeline.timelineFollow();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            quitApplication(e);
        }

        private void menuNewProject_Clicked(object sender, RoutedEventArgs e)
        {
            newProject();
        }

        private void menuOpenProject_Clicked(object sender, RoutedEventArgs e)
        {
            openProject();
        }

        private void menuSaveProject_Clicked(object sender, RoutedEventArgs e)
        {
            saveProject();
        }

        private void menuSaveAsProject_Clicked(object sender, RoutedEventArgs e)
        {
            saveAsProject();
        }

        private void menuCloseProject_Clicked(object sender, RoutedEventArgs e)
        {
            closeProject();
        }

        private void menu_settings_Click(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings(this);
            settings.Show();
        }

        private void menuQuit_Clicked(object sender, RoutedEventArgs e)
        {
            quitApplication(new CancelEventArgs());
        }

        /*Mise à jour manuelle de l'application*/
        private void menu_item_update_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://gitlab.com/deltacode/dubstudio");       
        }

        /*Clic sur le bouton de feedback*/
        private void menu_item_features_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://forms.gle/jLBrWdCcyFeNMsBu5");
        }

        /*Bouton de sélection d'une nouvelle vidéo sur le DSVideoPlayer*/
        private void video_player_VideoSelectClick()
        {
            selectVideo();
        }

        /*Boite de sélection d'une nouvelle vidéo*/
        public void selectVideo()
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = System.Windows.Application.Current.Resources["S_DIALOG_OPEN_VIDEO_TITLE"].ToString();
            openDialog.Filter = System.Windows.Application.Current.Resources["S_DIALOG_OPEN_VIDEO_FILTER"].ToString() + " | *.avi; *.mp4; *.wmv; *.mpg; *.mpeg; *.aiff; *.mov; *.m4v; *.mp4v; *.3gpp; *.mkv;";
            openDialog.Multiselect = false;
            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                _project.setVideoPath(new Uri(openDialog.FileName, UriKind.Absolute));
                video_player.loadVideo(_project.getVideoPath());
                if (!timer.IsEnabled) timer.Start();
            }       
        }

        /*Modèles pour l'affichage des personnages*/
        public class DataCharacter
        {
            public int dataCharacterId { set; get; }
            public string dataCharacterNom { set; get; }
            public SolidColorBrush dataCharacterRGB { get; set; }
            public List<string> dataCharacterRacc { get; set; }
            public string dataCharacterShortcut { get; set; }

            public DataCharacter(int id, string name, SolidColorBrush color, string shortcut)
            {
                dataCharacterId = id;
                dataCharacterNom = name;
                dataCharacterRGB = color;
                if (shortcut == null || shortcut == "") dataCharacterShortcut = (string)System.Windows.Application.Current.FindResource("s_main_none");
                else dataCharacterShortcut = shortcut;
                dataCharacterRacc = new List<string> { (string)System.Windows.Application.Current.FindResource("s_main_none"), "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12" };
            }

            public SolidColorBrush getColor()
            {
                return dataCharacterRGB;
            }
        }

        /*Modèles pour l'affichage des textes*/
        public class DataReplica
        {
            public int dataReplicaId { set; get; }
            public string dataReplicaName { set; get; }
            public string dataReplicaCharacter { set; get; }
            public string dataReplicaStart { set; get; }
            public string dataReplicaEnd { set; get; }
            public List<String> dataReplicaCharacters { get; set; }
            public static List<String> charactersList { get; set; }

            public DataReplica(int id, string name, string character, string start, string end)
            {
                dataReplicaId = id;
                dataReplicaCharacters = charactersList;
                dataReplicaName = name;
                dataReplicaCharacter = character;
                dataReplicaStart = start;
                dataReplicaEnd = end;
            }
            public static void updateCharacters(List<Character> characters)
            {
                charactersList = new List<String> { };
                foreach (Character a in characters)
                {
                    charactersList.Add(a.getName());
                }
            }
        }
    }
}
