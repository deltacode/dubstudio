﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Navigation;
using System.IO;
using BrendanGrant.Helpers.FileAssociation;
using IWshRuntimeLibrary;
using DubStudio;
using Microsoft.Win32;

namespace DubStudio
{
    /// <summary>
    /// Logique d'interaction pour Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private DSWindow mainWindow;
        public Settings(DSWindow e)
        {
            InitializeComponent();
            mainWindow = e;

            /*Initialisation onglet Général*/
            cb_language.Items.Clear();
            foreach(string s in App.fetchLanguages())
            {
                cb_language.Items.Add(s.Substring(10, 5));
            }
            cb_language.SelectedItem = Properties.Settings.Default.language;

            FileAssociationInfo fai = new FileAssociationInfo(".dub");
            if (fai.Exists)
            {
                ProgramAssociationInfo pai = new ProgramAssociationInfo(fai.ProgID);
                if (pai.Exists) ckb_file_association.IsChecked = true;
                else ckb_file_association.IsChecked = false;
            }
            else ckb_file_association.IsChecked = false;

            ckb_startup_last_project.IsChecked = Properties.Settings.Default.startWithRecentProject;
            tb_recent_projects_buffer.Text = Properties.Settings.Default.recentProjectsBuffer.ToString();
            bool crashReporter = Properties.Settings.Default.crashReporter;
            cb_send_report.SelectedIndex = (crashReporter) ? 1 : 0;
            /*Initialisation onglet Interface*/
            cb_theme.SelectedIndex = Properties.Settings.Default.theme;
            /*Initialisation onglet Sauvegarde*/
            cb_save_interval.Text = Properties.Settings.Default.automaticSave.ToString();
            lbl_installed_version.Content = Properties.Settings.Default.version;
        }
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void btn_default_Click(object sender, RoutedEventArgs e)
        {
            /*Restaurer le logiciel aux réglages d'usine*/
            if (MessageBox.Show(Application.Current.Resources["S_DIALOG_DEFAULT_SETTINGS"].ToString(), Application.Current.Resources["S_TITLE_DEFAULT_SETTINGS"].ToString(), MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                cb_language.SelectedItem = "en-US";
                ckb_file_association.IsChecked = true;
                ckb_startup_last_project.IsChecked = false;
                tb_recent_projects_buffer.Text = "3";
                cb_send_report.SelectedIndex = 0;
                cb_theme.SelectedIndex = 0;
                cb_save_interval.Text = "15";
            }
        }

        /*Sauvegarde des paramètres*/
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            /*Onglet Général*/
            Properties.Settings.Default.language = cb_language.SelectedItem.ToString();
            App app = (App) Application.Current;
            app.changeLanguage(Properties.Settings.Default.language);

            if ((bool)ckb_file_association.IsChecked)
            {
                //Association des fichiers .dub à l'application
                FileAssociationInfo fai = new FileAssociationInfo(".dub");
                if (!fai.Exists) fai.Create("DubStudio");
                ProgramAssociationInfo pai = new ProgramAssociationInfo(fai.ProgID);
                if (!pai.Exists) pai.Create("DubStudio Project", new ProgramVerb("Open", System.Windows.Forms.Application.ExecutablePath + " %1"));
            }
            else
            {
                //Suppression des clés registre
                FileAssociationInfo fai = new FileAssociationInfo(".dub");
                ProgramAssociationInfo pai = new ProgramAssociationInfo(fai.ProgID);
                pai.RemoveVerb(new ProgramVerb("Open", System.Windows.Forms.Application.ExecutablePath + " %1"));
                pai.Delete();
                fai.Delete();
            }
            Properties.Settings.Default.startWithRecentProject = (bool)ckb_startup_last_project.IsChecked;
            int recentBuffer = Properties.Settings.Default.recentProjectsBuffer;
            if (int.Parse(tb_recent_projects_buffer.Text) != recentBuffer) Properties.Settings.Default.recentProjects = new System.Collections.Specialized.StringCollection();
            Properties.Settings.Default.recentProjectsBuffer = int.Parse(tb_recent_projects_buffer.Text);
            int crashReporter = cb_send_report.SelectedIndex;
            Properties.Settings.Default.crashReporter = (crashReporter == 1) ? true : false;
            /*Onglet interface*/
            Properties.Settings.Default.theme = cb_theme.SelectedIndex;
            /*Onglet Sauvegarde*/
            Properties.Settings.Default.automaticSave = int.Parse(cb_save_interval.Text);
            Properties.Settings.Default.Save();
            MessageBox.Show((string)System.Windows.Application.Current.Resources["S_DIALOG_PREFERENCES_SAVED_MESSAGE"], (string)System.Windows.Application.Current.Resources["S_DIALOG_PREFERENCES_SAVED_TITLE"], MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://gitlab.com/deltacode/dubstudio");
        }

        private void btn_changelog_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("Changelog.txt");
        }
    }
}
