﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DubStudio
{
    public class Project
    {
        public static int _projectsId = 0;

        private int _id;
        private String _name;
        private String _path;
        private String _designation;
        private String _author;
        private String _version;
        private Uri _videoPath;
        private bool _hasUnsavedChanges = true;
        private List<Actor> _actors;

        public Project()
        {
            _id = _projectsId++;
            _name = Application.Current.Resources["S_PROJECT_BLANK"].ToString();
            string path = "tmp_" + generateHexa() + ".dub";
            string executable = System.Windows.Forms.Application.StartupPath;
            _path = Path.Combine(executable, "Temp", path);
            Directory.CreateDirectory(Path.Combine(executable, "Temp"));
            File.Create(_path);
            _designation = Application.Current.Resources["S_PROJECT_DESCRIPTION"].ToString();
            _author = Application.Current.Resources["S_PROJECT_AUTHOR"].ToString();
            _version = "1.0.0";
            //_videoPath = new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "video.avi"));
            _videoPath = null;

            _actors = new List<Actor> { };
        }

        public int getId()
        {
            return _id;
        }

        public String getName()
        {
            return _name;
        }

        public void setName(String name)
        {
            _name = name;
        }

        public String getPath()
        {
            return _path;
        }

        public void setPath(String path)
        {
            _path = path;
        }

        public String getDesignation()
        {
            return _designation;
        }

        public void setDesignation(String designation)
        {
            _designation = designation;
        }

        public String getAuthor()
        {
            return _author;
        }

        public void setAuthor(String author)
        {
            _author = author;
        }

        public String getVersion()
        {
            return this._version;
        }

        public void setVersion(String version)
        {
            this._version = version;
        }

        public Uri getVideoPath()
        {
            return _videoPath;
        }

        public void setVideoPath(Uri videoPath)
        {
            _videoPath = videoPath;
        }

        public List<Actor> getActors()
        {
            return this._actors;
        }

        public void setActors(List<Actor> actors)
        {
            this._actors = actors;
        }

        public void clearActors()
        {
            this._actors.Clear();
        }

        public void addActor(Actor actor)
        {
            this._actors.Add(actor);
        }

        public void removeActor(Actor actor)
        {
            this._actors.Remove(actor);
        }

        public bool hasUnsavedChanges()
        {
            return _hasUnsavedChanges;
        }

        public void hasUnsavedChanges(bool changes)
        {
            _hasUnsavedChanges = changes;
        }

        /*Génére une chaîne en hexadécimal*/
        public string generateHexa()
        {
            Random random = new Random();
            int num = random.Next();
            return num.ToString();
        }        
    }
}
