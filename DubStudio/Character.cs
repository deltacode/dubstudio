﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows;

namespace DubStudio
{
    public class Character
    {
        public static int _charactersId = 0;

        private int _id;
        private string _name;
        private Gender _gender;
        private int _age;
        private System.Windows.Media.Color _color;
        private List<Replica> _replicas;

        public enum Gender
        {
            MALE = 0,
            FEMALE = 1
        }

        /*Builders*/
        public Character()
        {
            _id = _charactersId++;
            _name = Application.Current.Resources["S_CHARACTER_BLANK"].ToString() + " " + _id;
            _gender = Gender.MALE;
            _age = 0;
            Random rnd = new Random();
            _color = System.Windows.Media.Color.FromRgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            _replicas = new List<Replica> { };
        }

        public Character(String name, Gender gender, int age, System.Windows.Media.Color color)
        {
            _id = _charactersId++;
            _name = name;
            _gender = gender;
            _age = age;
            _color = color;
            _replicas = new List<Replica> { };
        }

        public Character(String name, Gender gender, int age, System.Windows.Media.Color color, List<Replica> replicas)
        {
            _id = _charactersId++;
            _name = name;
            _gender = gender;
            _age = age;
            _color = color;
            _replicas = replicas;
        }

        /*Methods*/
        public int getId()
        {
            return _id;
        }

        public string getName()
        {
            return _name;
        }

        public void setName(string name)
        {
            _name = name;
        }

        public Gender getGender()
        {
            return this._gender;
        }

        public void setGender(Gender gender)
        {
            this._gender = gender;
        }

        public int getAge()
        {
            return _age;
        }

        public void setAge(int age)
        {
            this._age = age;
        }

        public System.Windows.Media.Color getColor()
        {
            return _color;
        }

        public void setColor(System.Windows.Media.Color color)
        {
            _color = color;
        }

        public List<Replica> getReplicas()
        {
            return this._replicas;
        }

        public void setReplicas(List<Replica> replicas)
        {
            this._replicas = replicas;
        }

        public void clearReplicas()
        {
            this._replicas.Clear();
        }

        public void addReplica(Replica replica)
        {
            this._replicas.Add(replica);
        }

        public void removeReplica(Replica replica)
        {
            this._replicas.Remove(replica);
        }
    }
}
