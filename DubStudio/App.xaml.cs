﻿using Sentry;
using Sentry.Protocol;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace DubStudio
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ResourceDictionary obj;

        public App()
        {
            if (DubStudio.Properties.Settings.Default.crashReporter)
            {
                SentrySdk.Init("https://0a7d8a795e894e27a49822e9d665f769@sentry.io/5188905");
                SentrySdk.ConfigureScope(scope =>
                {
                    scope.User = new Sentry.Protocol.User
                    {
                        Email = "user@dubstudio",
                        Username = "Anonymous"
                    };
                    scope.SetTag("version", DubStudio.Properties.Settings.Default.version);
                    scope.SetTag("langue", DubStudio.Properties.Settings.Default.language);
                });
            }
        }

        /*On récupère au démarrage le dictionnaire de langue par défaut*/
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            foreach (ResourceDictionary item in this.Resources.MergedDictionaries)
            {
                if (item.Source != null && item.Source.OriginalString.Contains(@"Languages\")) obj = item;
            }
            DSWindow win = new DSWindow();
            if(e.Args.Length == 1) win = new DSWindow(e.Args[0]);
            win.Show();
        }

        /*Changement de la langue du logiciel*/
        public void changeLanguage(String language)
        {
            Uri dictionaryUri = new Uri(String.Format("/DubStudio;component/Languages/{0}.xaml", language), UriKind.Relative);
            if (String.IsNullOrEmpty(dictionaryUri.OriginalString) == false)
            {
                ResourceDictionary objNewLanguageDictionary = (ResourceDictionary)(LoadComponent(dictionaryUri));
                if (objNewLanguageDictionary != null)
                {
                    this.Resources.MergedDictionaries.Remove(obj);
                    this.Resources.MergedDictionaries.Add(objNewLanguageDictionary);
                    CultureInfo culture = new CultureInfo((string)Application.Current.Resources["S_CULTURE"]);
                    Thread.CurrentThread.CurrentCulture = culture;
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
            }
        }

        /*Récupération des fichiers de ressources langagières*/
        public static string[] fetchLanguages()
        {
            return Directory.GetFiles("Languages");
        }
    }
}
