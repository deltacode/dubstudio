﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DubStudio.DSComponents
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class DSMediaPlayer : UserControl
    {
        private bool _isPlaying = false;
        private bool _isDragging = false;
        private Uri _videoPath = null;
        private List<TimeSpan> _markers = new List<TimeSpan> { };

        /*Constrcuteur de DSMediaPlayer*/
        public DSMediaPlayer()
        {
            InitializeComponent();
            //Désactivation des contrôles
            enablingControls(false);
        }

        /*Retourne si une vidéo est en cours de lecture ou non*/
        public bool isPlaying()
        {
            return _isPlaying;
        }

        /*Retourne la position actuelle de la vidéo*/
        public TimeSpan getVideoPosition()
        {
            if (DSMediaPlayer_media.NaturalDuration.HasTimeSpan) return DSMediaPlayer_media.Position;
            else return new TimeSpan(-1);
        }

        /*Retourne le chemin de la vidéo en cours de visualisation*/
        public Uri getVideoPath()
        {
            return _videoPath;
        }

        /*Charge une vidéo dans le MediaElement*/
        public void loadVideo(Uri path)
        {
            try
            {
                _videoPath = path;
                DSMediaPlayer_media.Source = _videoPath;
                playPauseVideo();
                stopVideo();
            }
            catch (Exception e)
            {
                MessageBox.Show("An error has occured when trying to open this media file.\nAdditional information : " + e.Message);
            }
        }

        /*La vidéo a été ouverte par le lecteur embarqué*/
        private void DSMediaPlayer_media_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (DSMediaPlayer_media.NaturalDuration.HasTimeSpan)
            {
                setDuration(DSMediaPlayer_media.NaturalDuration.TimeSpan);
                DSMediaPlayer_selectVideo.Visibility = Visibility.Hidden;
                enablingControls(true);
            }
        }

        /*Mise à jour de la valeur du curseur et du label de position*/
        public void updateVideoTime()
        {
            if (DSMediaPlayer_media.Source != null)
            {
                if (!_isDragging)
                {
                    setCurrentDate(DSMediaPlayer_media.Position);
                    DSMediaPlayer_slider.Value = DSMediaPlayer_media.Position.Ticks;
                }
                //L'utilisateur est en train de déplacer le curseur, on affiche la date actuelle
                else setCurrentDate(new TimeSpan((long)DSMediaPlayer_slider.Value));
                //Si on est à la fin de la vidéo, on opère une opération stop
                if (DSMediaPlayer_media.NaturalDuration.HasTimeSpan)
                {
                    if (DSMediaPlayer_media.Position == DSMediaPlayer_media.NaturalDuration) stopVideo();
                }
            }
        }

        /*Clic sur le bouton Play/Pause*/
        private void DSMediaPlayer_btnPlay_Clicked(object sender, RoutedEventArgs e)
        {
            playPauseVideo();
        }

        /*Clic sur le bouton Stop*/
        private void DSMediaPlayer_btnStop_Clicked(object sender, RoutedEventArgs e)
        {
            stopVideo();
        }

        /*Clic sur le bouton Aller en arrière*/
        private int beforeSpeed = 0;
        private DispatcherTimer timerBefore = new DispatcherTimer();
        private void DSMediaPlayer_btnBefore_Clicked(object sender, RoutedEventArgs e)
        {
            switch (beforeSpeed)
            {
                case 0:
                    if (timerBefore.IsEnabled) timerBefore.Stop();
                    DSMediaPlayer_imageBefore.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed1.png", UriKind.Relative));
                    DSMediaPlayer_btnBefore.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_BEFORE_SPEED1_TOOLTIP"];
                    beforeSpeed = 1;
                    timerBefore.Interval = TimeSpan.FromMilliseconds(250);
                    timerBefore.Tick += goBack;
                    timerBefore.Start();
                    break;
                case 1:
                    if (timerBefore.IsEnabled) timerBefore.Stop();
                    DSMediaPlayer_imageBefore.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed2.png", UriKind.Relative));
                    DSMediaPlayer_btnBefore.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_BEFORE_SPEED2_TOOLTIP"];
                    beforeSpeed = 2;
                    timerBefore.Interval = TimeSpan.FromMilliseconds(250);
                    timerBefore.Tick += goBack;
                    timerBefore.Start();
                    break;
                case 2:
                    if (timerBefore.IsEnabled) timerBefore.Stop();
                    DSMediaPlayer_imageBefore.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed3.png", UriKind.Relative));
                    DSMediaPlayer_btnBefore.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_BEFORE_SPEED3_TOOLTIP"];
                    beforeSpeed = 3;
                    timerBefore.Interval = TimeSpan.FromMilliseconds(250);
                    timerBefore.Tick += goBack;
                    timerBefore.Start();
                    break;
                default:
                    if (timerBefore.IsEnabled) timerBefore.Stop();
                    DSMediaPlayer_imageBefore.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/before.png", UriKind.Relative));
                    DSMediaPlayer_btnBefore.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_BEFORE_TOOLTIP"];
                    beforeSpeed = 0;
                    break;
            }
        }

        /*Aller vers l'arrière à la vitesse désirée*/
        private void goBack(object sender, EventArgs e)
        {
            switch(beforeSpeed)
            {
                case 2:
                    if (DSMediaPlayer_media.Position - new TimeSpan(0, 0, 10) > new TimeSpan(0, 0, 0)) DSMediaPlayer_media.Position -= new TimeSpan(0, 0, 10);
                    else stopAccelerateBefore();
                    break;
                case 3:
                    if (DSMediaPlayer_media.Position - new TimeSpan(0, 0, 30) > new TimeSpan(0, 0, 0)) DSMediaPlayer_media.Position -= new TimeSpan(0, 0, 30);
                    else stopAccelerateBefore();
                    break;
                default:
                    if (DSMediaPlayer_media.Position - new TimeSpan(0, 0, 5) > new TimeSpan(0, 0, 0)) DSMediaPlayer_media.Position -= new TimeSpan(0, 0, 5);
                    else stopAccelerateBefore();
                    break;
            }
        }

        /*On arrête le retour en arrière lorsque la souris sort du bouton*/
        private void DSMediaPlayer_btnBefore_MouseLeave(object sender, MouseEventArgs e)
        {
            stopAccelerateBefore();
        }

        /*On arrête la lecture en accéléré*/
        private void stopAccelerateBefore()
        {
            if (timerBefore.IsEnabled) timerBefore.Stop();
            DSMediaPlayer_imageBefore.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/before.png", UriKind.Relative));
            DSMediaPlayer_btnBefore.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_BEFORE_TOOLTIP"];
            beforeSpeed = 0;
        }

        /*Clic sur le bouton Aller en avant*/
        private int afterSpeed = 0;
        private DispatcherTimer timerAfter = new DispatcherTimer();
        private void DSMediaPlayer_btnAfter_Clicked(object sender, RoutedEventArgs e)
        {
            switch (afterSpeed)
            {
                case 0:
                    if (timerAfter.IsEnabled) timerAfter.Stop();
                    DSMediaPlayer_imageAfter.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed1.png", UriKind.Relative));
                    DSMediaPlayer_btnAfter.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_AFTER_SPEED1_TOOLTIP"];
                    afterSpeed = 1;
                    timerAfter.Interval = TimeSpan.FromMilliseconds(250);
                    timerAfter.Tick += goAhead;
                    timerAfter.Start();
                    break;
                case 1:
                    if (timerAfter.IsEnabled) timerAfter.Stop();
                    DSMediaPlayer_imageAfter.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed2.png", UriKind.Relative));
                    DSMediaPlayer_btnAfter.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_AFTER_SPEED2_TOOLTIP"];
                    afterSpeed = 2;
                    timerAfter.Interval = TimeSpan.FromMilliseconds(250);
                    timerAfter.Tick += goAhead;
                    timerAfter.Start();
                    break;
                case 2:
                    if (timerAfter.IsEnabled) timerAfter.Stop();
                    DSMediaPlayer_imageAfter.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/speed3.png", UriKind.Relative));
                    DSMediaPlayer_btnAfter.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_AFTER_SPEED3_TOOLTIP"];
                    afterSpeed = 3;
                    timerAfter.Interval = TimeSpan.FromMilliseconds(250);
                    timerAfter.Tick += goAhead;
                    timerAfter.Start();
                    break;
                default:
                    if (timerAfter.IsEnabled) timerAfter.Stop();
                    DSMediaPlayer_imageAfter.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/after.png", UriKind.Relative));
                    DSMediaPlayer_btnAfter.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_AFTER_TOOLTIP"];
                    afterSpeed = 0;
                    break;
            }
        }

        /*Aller vers l'avant à la vitesse désirée*/
        private void goAhead(object sender, EventArgs e)
        {
            switch (afterSpeed)
            {
                case 2:
                    if (DSMediaPlayer_media.Position + new TimeSpan(0, 0, 10) <= DSMediaPlayer_media.NaturalDuration.TimeSpan) DSMediaPlayer_media.Position += new TimeSpan(0, 0, 10);
                    else stopAccelerateAfter();
                    break;
                case 3:
                    if (DSMediaPlayer_media.Position + new TimeSpan(0, 0, 30) <= DSMediaPlayer_media.NaturalDuration.TimeSpan) DSMediaPlayer_media.Position += new TimeSpan(0, 0, 30);
                    else stopAccelerateAfter();
                    break;
                default:
                    if (DSMediaPlayer_media.Position + new TimeSpan(0, 0, 5) <= DSMediaPlayer_media.NaturalDuration.TimeSpan) DSMediaPlayer_media.Position += new TimeSpan(0, 0, 5);
                    else stopAccelerateAfter();
                    break;
            }
        }

        /*On arrête le retour en arrière lorsque la souris sort du bouton*/
        private void DSMediaPlayer_btnAfter_MouseLeave(object sender, MouseEventArgs e)
        {
            stopAccelerateAfter();
        }

        /*On arrête la lecture en accéléré*/
        private void stopAccelerateAfter()
        {
            if (timerAfter.IsEnabled) timerAfter.Stop();
            DSMediaPlayer_imageAfter.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/after.png", UriKind.Relative));
            DSMediaPlayer_btnAfter.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_AFTER_TOOLTIP"];
            afterSpeed = 0;
        }

        /*Click sur le bouton Marker*/
        private void DSMediaPlayer_btnMarker_Clicked(object sender, RoutedEventArgs e)
        {
            addMarker();
        }

        /*Reprendre/Mettre en pause la lecture*/
        private void playPauseVideo()
        {
            if(_videoPath != null)
            {
                if (_isPlaying)
                {
                    DSMediaPlayer_media.Pause();
                    DSMediaPlayer_imagePlayPause.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/play.png", UriKind.Relative));
                    DSMediaPlayer_btnPlay.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_START_TOOLTIP"];
                }
                else
                {
                    DSMediaPlayer_media.Play();
                    DSMediaPlayer_imagePlayPause.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/pause.png", UriKind.Relative));
                    DSMediaPlayer_btnPlay.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_PAUSE_TOOLTIP"];
                }
                _isPlaying = !_isPlaying;
            }
        }

        /*Arrête la vidéo en cours et revient au début*/
        private void stopVideo()
        {
            if (_videoPath != null)
            {
                DSMediaPlayer_media.Stop();
                DSMediaPlayer_imagePlayPause.Source = new BitmapImage(new Uri("/DSComponents/DSMediaPlayer/images/64w/play.png", UriKind.Relative));
                DSMediaPlayer_btnPlay.ToolTip = Application.Current.Resources["S_MEDIA_PLAYER_START_TOOLTIP"];
            }
        }

        /*Ajoute la position actuelle comme marqueur*/
        public void addMarker()
        {
            if (_videoPath != null) _markers.Add(DSMediaPlayer_media.Position);
        }

        /*Affiche un timestamp dans le label de début*/
        public void setCurrentDate(TimeSpan current)
        {
            DSMediaPlayer_lblCurrent.Content = String.Format("{0}", current.ToString(@"hh\:mm\:ss\.fff"));
        }

        /*Affiche un timestamp dans le label de fin*/
        public void setDuration(TimeSpan total)
        {
            DSMediaPlayer_slider.Maximum = total.Ticks;
            DSMediaPlayer_lblTotal.Content = String.Format("{0}", total.ToString(@"hh\:mm\:ss\.fff"));
        }

        /*Libère le lecteur pour une prochaine vidéo*/
        public void resetVideo()
        {
            DSMediaPlayer_lblCurrent.Content = "-- : -- : -- . ---";
            DSMediaPlayer_lblTotal.Content = "-- : -- : -- . ---";
            stopVideo();
            DSMediaPlayer_media.Source = null;
            DSMediaPlayer_slider.Value = 0;
            DSMediaPlayer_slider.Maximum = 1;
            _videoPath = null;
            DSMediaPlayer_selectVideo.Visibility = Visibility.Visible;
        }

        /*Activation/Désactivation des contrôles*/
        public void enablingControls(bool enabling)
        {
            if (!enabling && DSMediaPlayer_media.HasVideo) resetVideo();
            DSMediaPlayer_btnPlay.IsEnabled = enabling;
            DSMediaPlayer_btnStop.IsEnabled = enabling;
            DSMediaPlayer_btnBefore.IsEnabled = enabling;
            DSMediaPlayer_btnAfter.IsEnabled = enabling;
            DSMediaPlayer_btnMarker.IsEnabled = enabling;
            DSMediaPlayer_slider.IsEnabled = enabling;
        }

        /*Gestion de la visibilité du sélecteur de vidéo*/
        public void showVideoSelector(bool enabling)
        {
            if (enabling) DSMediaPlayer_selectVideo.Visibility = Visibility.Visible;
            else DSMediaPlayer_selectVideo.Visibility = Visibility.Hidden;
        }

        /*L'utilisateur a commencé de bouger le slider*/
        private void DSMediaPlayer_slider_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            _isDragging = true;
        }

        /*L'utilisateur a terminé de bouger le slider*/
        private void DSMediaPlayer_slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            _isDragging = false;
            if (_videoPath != null) DSMediaPlayer_media.Position = new TimeSpan((long)DSMediaPlayer_slider.Value);
        }

        /*On crée l'événement UserVideoSelected lorsqu'on clique sur le bouton de sélection d'une vidéo*/
        public delegate void VideoSelectEventHandler();
        public event VideoSelectEventHandler VideoSelectClick;
        private void DSMediaPlayer_selectVideo_Click(object sender, RoutedEventArgs e)
        {
            if (VideoSelectClick != null) VideoSelectClick();
        }
    }
}