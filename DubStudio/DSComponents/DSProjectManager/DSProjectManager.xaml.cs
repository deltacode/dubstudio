﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DubStudio.DSComponents.DSProjectManager
{
    /// <summary>
    /// Logique d'interaction pour DSProjectManager.xaml
    /// </summary>
    public partial class DSProjectManager : UserControl
    {
        public DSProjectManager()
        {
            InitializeComponent();
        }
    }

    //public void btn_char_change_color_Click(object sender, RoutedEventArgs e)
    //{
    //    //Ouverture de la palette et sélection d'une couleur
    //    ColorDialog colorDialog = new ColorDialog();
    //    object row = ((System.Windows.Controls.Button)sender).CommandParameter;
    //    object background = ((System.Windows.Controls.Button)sender).Background;
    //    System.Drawing.Color back = System.Drawing.ColorTranslator.FromHtml(background.ToString());
    //    colorDialog.FullOpen = true;
    //    colorDialog.AnyColor = true;
    //    colorDialog.Color = back;

    //    if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
    //    {
    //        MessageBox.Show("La couleur de " + Character.getCharacters((int)row).getName() + " a été modifiée en " + colorDialog.Color);
    //    }
    //}
    //public void datagrid_goto_location(object sender, RoutedEventArgs e)
    //{
    //    //Définit la position dans la vidéo
    //    object row = ((System.Windows.Controls.Button)sender).CommandParameter;
    //    object locationString = ((System.Windows.Controls.Button)sender).Content;
    //    video_player.Position = TimeSpan.Parse((String)locationString);
    //}

    ////Ajout/Suppression des éléments au projet
    //private void tc_btn_add_char_Click(object sender, RoutedEventArgs e)
    //{
    //    AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Character);
    //    box.Show();
    //}
    //private void tc_btn_add_scene_Click(object sender, RoutedEventArgs e)
    //{
    //    AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Scene);
    //    box.Show();
    //}
    //private void tc_btn_add_text_Click(object sender, RoutedEventArgs e)
    //{
    //    AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Text);
    //    box.Show();
    //}
    //private void tc_btn_delete_char_Click(object sender, RoutedEventArgs e)
    //{
    //    //Suppression du personnage sélectionné
    //    int id = data_characters.SelectedIndex;
    //    if (id != -1)
    //    {
    //        Action.addAction(new Action("P", Character.getCharacters(id), Action.Type.Deletion, false), this);
    //    }
    //    else MessageBox.Show("Sélectionnez d'abord un personnage et réessayez");

    //}
    //private void tc_btn_delete_text_Click(object sender, RoutedEventArgs e)
    //{
    //    //Suppression de la réplique sélectionnée
    //    int id = data_texts.SelectedIndex;
    //    Action.addAction(new Action("T", Replica.getTexts(id), Action.Type.Deletion, false), this);
    //}
    //private void tc_btn_edit_char_Click(object sender, RoutedEventArgs e)
    //{
    //    int id = data_characters.SelectedIndex;
    //    AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Character, Character.getCharacters(id));
    //    box.Show();
    //}
    //private void tc_btn_edit_text_Click(object sender, RoutedEventArgs e)
    //{
    //    int id = data_texts.SelectedIndex;
    //    AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Text, Replica.getTexts(id));
    //    box.Show();
    //}

    //private void data_characters_Double_Click(object sender, RoutedEventArgs e)
    //{
    //    int id = data_characters.SelectedIndex;
    //    if (id != -1)
    //    {
    //        AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Character, Character.getCharacters(id));
    //        box.Show();
    //    }
    //}
    //private void data_scenes_Double_Click(object sender, RoutedEventArgs e)
    //{
    //    int id = data_scenes.SelectedIndex;
    //    if (id != -1)
    //    {
    //        AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Scene, Scene.getScenes(id));
    //        box.Show();
    //    }
    //}

    //private void data_texts_Double_Click(object sender, RoutedEventArgs e)
    //{
    //    int id = data_texts.SelectedIndex;
    //    if (id != -1)
    //    {
    //        AddActionBox box = new AddActionBox(this, AddActionBox.ElementType.Text, Replica.getTexts(id));
    //        box.Show();
    //    }
    //}
}
