﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DubStudio.DSComponents
{
    /// <summary>
    /// Logique d'interaction pour DSTimeline.xaml
    /// </summary>
    public partial class DSTimeline : UserControl
    {
        public DSTimeline()
        {
            InitializeComponent();
        }

        /*On fait suivre le curseur lorsque la position du slider est modifiée*/
        private void DSTimeline_slider_cursor_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            timelineFollow();
        }

        /*Modifie les marges du curseur pour faire coincider sa position avec celle du curseur*/
        public void timelineFollow()
        {
            double newMargin = ((DSTimeline_slider_cursor.ActualWidth - 10) * DSTimeline_slider_cursor.Value) / DSTimeline_slider_cursor.Maximum;
            DSTimeline_cursor.Margin = new Thickness(newMargin, 0, 0, 0);
        }

        /*Met à jour les éléments des pistes*/
        public void synchronizeReplicas(TimeSpan videoPosition, List<Actor> actors)
        {
            clearTracks();

            //Détermination de la position de lecture, de la position minimale et maximale à afficher
            long tickResolution = (long)DSTimeline_scale.Value;
            long tickPlay = (long)DSTimeline_slider_cursor.Value;
            long tickMin = (long)(videoPosition.Ticks - (tickPlay - DSTimeline_slider_cursor.Minimum) * tickResolution);
            long tickMax = (long)(videoPosition.Ticks + (DSTimeline_slider_cursor.Maximum - tickPlay) * tickResolution);

            //Vérifier quelles répliques se trouvent dans l'intervalle de la timeline
            foreach(Actor act in actors)
            {
                foreach(Character cha in act.getCharacters())
                {
                    foreach(Replica rep in cha.getReplicas())
                    {
                        if (rep.getStart().Ticks < tickMax && rep.getEnd().Ticks > tickMin)
                        {
                            //Affichage de la réplique
                            TextBlock t = new TextBlock();
                            t.Text = rep.getText();
                            t.FontFamily = new FontFamily("Comfortaa");
                            t.Name = "replica_" + rep.getId().ToString();
                            t.Foreground = new SolidColorBrush(cha.getColor());
                            //t.MouseLeftButtonUp += new MouseButtonEventHandler(label_Click);
                            long width = (long)DSTimeline_disposer.ActualWidth;
                            long margin = ((rep.getStart().Ticks - tickMin) * width) / (tickMax - tickMin);
                            Viewbox scaleBox = new Viewbox();
                            scaleBox.Margin = new Thickness(margin, 5, 0, 5);
                            scaleBox.VerticalAlignment = VerticalAlignment.Stretch;
                            scaleBox.HorizontalAlignment = HorizontalAlignment.Left;
                            scaleBox.StretchDirection = StretchDirection.Both;
                            scaleBox.Stretch = Stretch.Fill;
                            scaleBox.Child = t;
                            scaleBox.Width = (((rep.getEnd() - rep.getStart()).Ticks) * width) / (tickMax - tickMin);
                            if (rep.getTrack() == 1) DSTimeline_track1.Children.Add(scaleBox);
                            else if (rep.getTrack() == 2) DSTimeline_track2.Children.Add(scaleBox);
                            else if (rep.getTrack() == 3) DSTimeline_track3.Children.Add(scaleBox);
                        }
                    }
                }
            }
        }

        /*Activation/Désactivation des contrôles*/
        public void enablingControls(bool enabling)
        {
            clearTracks();
            if (enabling) DSTimeline_slider_cursor.Value = DSTimeline_slider_cursor.Maximum / 2;
            else DSTimeline_slider_cursor.Value = 0;
            DSTimeline_slider_cursor.IsEnabled = enabling;
            DSTimeline_scale.IsEnabled = enabling;
            timelineFollow();
        }

        /*Nettoyage des pistes*/
        public void clearTracks()
        {
            DSTimeline_track1.Children.Clear();
            DSTimeline_track2.Children.Clear();
            DSTimeline_track3.Children.Clear();
        }
    }
}
