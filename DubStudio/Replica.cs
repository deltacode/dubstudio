﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DubStudio
{
    public class Replica
    {
        public static int _replicasId = 0;

        private int _id;
        private string _text;
        private int _track;
        private TimeSpan _start;
        private TimeSpan _end;

        /*Builders*/
        public Replica(string text, int track, TimeSpan start, TimeSpan end)
        {
            _id = _replicasId++;
            _text = text;
            _track = track;
            _start = start;
            _end = end;
        }

        /*Methods*/
        public int getId()
        {
            return _id;
        }

        public string getText()
        {
            return _text;
        }

        public void setText(string text)
        {
            _text = text;
        }
        
        public int getTrack()
        {
            return _track;
        }
        public void setTrack(int track)
        {
            _track = track;
        }
        public TimeSpan getStart()
        {
            return _start;
        }
        public void setStart(TimeSpan start)
        {
            _start = start;
        }
        public TimeSpan getEnd()
        {
            return _end;
        }
        public void setEnd(TimeSpan end)
        {
            _end = end;
        }
    }
}
